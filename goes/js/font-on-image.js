window.addEventListener('load', function () {
	getPairs();
});

var topologyInfo = [];

function getPairs() {

	var http = new XMLHttpRequest();
	var url = "https://5g2zpa3jfd.execute-api.ap-southeast-2.amazonaws.com/prod/pair-list/";
	http.open("GET", url, true);

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			topologyInfo = JSON.parse(http.responseText);

			// console.log(topologyInfo.length);
			// console.log(topologyInfo[0]);
			// console.log(topologyInfo);

			cycleColors();
			setInterval(cycleColors, 8000);
		}
	}
	http.send();
}

function cycleColors() {

	for (var i = 0; i < 5; i++) {
		var font = getRandomColor();
		var background = getRandomColor();
		var example = document.getElementById('example_' + i);
		var score = scoreExample(font, background);

		for (var j = 0; j < example.childNodes.length; j++) {
			if (example.childNodes[j].className  == "example-color") {
				example.childNodes[j].style.background = '#' + background;
				example.childNodes[j].style.color = '#' + font;
			}
			if (example.childNodes[j].className  == "example-info") {

				example.childNodes[j].innerHTML  = 'Background = #' + background + '<br/>';
				example.childNodes[j].innerHTML += 'Font       = #' + font + '<br/>';
				example.childNodes[j].innerHTML += 'Score      = ' + score.toFixed(4);
			}
		}

	}

}

function scoreExample(font, background) {

	var allVotes = 1; //init to 1, very bayes
	var goodVotes = 1;
	topologyInfo.forEach(
		function(sample) {

			var weight = weighting(
				distance(
					{
						font: hexToRGB(font),
						background: hexToRGB(background)
					},
					sample.refValue
				)
			);
			allVotes += weight;
			goodVotes += weight * scoreToPercentage(sample.score);
		}
	);
	// console.log(allVotes);
	// console.log(goodVotes);
	// console.log(goodVotes / allVotes);
	// console.log(ratioToScore(goodVotes, allVotes - goodVotes));
	// console.log('---------');
	
	return ratioToScore(goodVotes, allVotes - goodVotes);

}

//get a base16 value between '00' and 'FF'
function getRandomHex() {
	var integer = Math.floor(Math.random() * 256);

	//convert to base16 padded with '0's if required
	return ("00" + integer.toString(16)).substr(-2,2);
}

function getRandomColor() {
	return getRandomHex() + getRandomHex() + getRandomHex();
}





// 'use strict';

// const doc = require('dynamodb-doc');

// const dynamo = new doc.DynamoDB();

// var lambdaResponse = [];

// exports.handler = (data, context, callback) => {
    
    // dynamo.scan({ TableName: 'goes_pair' }, context.done);
    // lambdaResponse = [];
    // dynamo.scan({ TableName: 'goes_pair' }, main.bind(context));
    
// }

//----------------------------------------------------------------------------------------------------------------------

function hexToRGB (hex) {
	return {
		r : parseInt(hex.substr(0, 2), 16),
		g : parseInt(hex.substr(2, 2), 16),
		b : parseInt(hex.substr(4, 2), 16)
	}
}


function main(err, response) {
	if (err) {
        console.log(err, err.stack);
        return [];
    }
    // console.log(response["Items"][0]);
    

    
	const refValues = createReferenceValues();
	refValues.forEach(
		function(refValue) {
				
			var weightedGoodVotes = 1; //init to 1, very bayes
			var weightedBadVotes = 1;
			response["Items"].forEach(
				function(vote) {
					weightedGoodVotes += weighting(
						distance(
							{
								font: hexToRGB(vote.good.font),
								background: hexToRGB(vote.good.background)
							},
							refValue
						)
					);
					weightedBadVotes += weighting(
						distance(
							{
								font: hexToRGB(vote.bad.font),
								background: hexToRGB(vote.bad.background)
							},
							refValue
						)
					);
					// console.log( 'good font = ' + hexToRGB(vote.good.font).r );
					// console.log( 'good background = ' + hexToRGB(vote.good.background).r );
					// console.log( 'bad font = ' + hexToRGB(vote.bad.font).r );
					// console.log( 'bad background = ' + hexToRGB(vote.bad.background).r );
				}
			);
			lambdaResponse.push(
			    {
			        'score' : ratioToScore(weightedGoodVotes, weightedBadVotes),
			        'refValue' : refValue
			    }
			);
			// console.log('--------------------------------------------------------');
			// console.log(refValue);
			// console.log('weightedGoodVotes = ' + weightedGoodVotes);
			// console.log('weightedBadVotes = ' + weightedBadVotes);
			// console.log('Score = ' + ratioToScore(weightedGoodVotes, weightedBadVotes));
		}
	);
	console.log('this many response = ' + lambdaResponse.length)
	this.done(null, lambdaResponse);
		// console.log(createReferenceValues());
}

const subLevels = ['20','60','A0','E0'];

function addDimension(set){
	var returnSet = [];
	set.forEach(
		function(item){
			subLevels.forEach(
				function(subLevel){
					returnSet.push(item + subLevel);
				}
			);
		}
	);
	return returnSet;
}

function createReferenceValues(){
	var set = subLevels;
	for (var i = 0; i < 5; i++) {
		set = addDimension(set);
	}
	var returnItems = [];
	set.forEach(
		function(item){
			returnItems.push({
				font: hexToRGB(item.substr(0, 6)),
				background: hexToRGB(item.substr(6, 6))
			});
		}
	);
	console.log('this many = ' + returnItems.length);
	return returnItems;

}

function distance(a, b){
	var total = 0;
	total += Math.pow(a.font.r - b.font.r, 2);
	total += Math.pow(a.font.g - b.font.g, 2);
	total += Math.pow(a.font.b - b.font.b, 2);
	total += Math.pow(a.background.r - b.background.r, 2);
	total += Math.pow(a.background.g - b.background.g, 2);
	total += Math.pow(a.background.b - b.background.b, 2);
	return Math.pow(total, 0.5);
}

function weighting(distance) {
	const magic = 90;
	var returnValue = magic - distance;
	if (returnValue < 0) {
		return 0;
	}
	return returnValue / magic;
}

//base 2 log of the ratio of good to bad
function ratioToScore(good, bad) {
	return Math.log(good / bad) / Math.log(2);
}

//returns expected percentage of good votes
function scoreToPercentage(score) {
	var ratio = Math.pow(2,score);
	return ratio / (ratio + 1);
}

