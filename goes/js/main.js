goodSamples = [];
badSamples = [];

const maxSamplesToHold = 10;

var lhs_font = '';
var rhs_font = '';
var lhs_background = '';
var rhs_background = '';

window.addEventListener('load', function () {
	lhs = document.getElementById('lhs');
	rhs = document.getElementById('rhs');
	lhs.onclick = voteLeft;
	rhs.onclick = voteRight;

	bothGood = document.getElementById('both-good');
	bothBad  = document.getElementById('both-bad');
	bothGood.onclick = voteBothGood;
	bothBad.onclick  = voteBothBad;

	cycleColors();
});

function voteLeft() {
	//build 'vote' object
	var vote = {
		good : {
			font : lhs_font,
			background : lhs_background
		},
		bad : {
			font : rhs_font,
			background : rhs_background
		}
	};
	submitVote([vote]);
	cycleColors();
}

function submitVote(votes) {
	//Submit vote to the server and cycle colors
	var params = JSON.stringify(votes);
	// console.log(votes);
	var http = new XMLHttpRequest();
	var url = "https://5g2zpa3jfd.execute-api.ap-southeast-2.amazonaws.com/prod/vote-result/";
	http.open("POST", url, true);

	http.setRequestHeader("Content-type", "application/json");

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			console.log(http.responseText);
		}
	}
	http.send(params);

	var center = document.getElementById('mid');
	center.innerHTML = 'vote submitted';
}

function voteRight() {
	//build 'vote' object
	var vote = {
		good : {
			font : rhs_font,
			background : rhs_background
		},
		bad : {
			font : lhs_font,
			background : lhs_background
		}
	};
	submitVote([vote]);
	cycleColors();
}

function voteBothBad() {
	var votes = [];
	var badSampleL = {
		font : lhs_font,
		background : lhs_background
	};
	var badSampleR = {
		font : rhs_font,
		background : rhs_background
	};

	//push the 'cartesian product' of the two new bad samples and existing good samples onto 'votes'
	goodSamples.forEach(
		function(goodSample) {
			votes.push ({
				good : goodSample,
				bad : badSampleL
			});

			votes.push ({
				good : goodSample,
				bad : badSampleR
			});
		}
	);

	//submit to the server that these colors are worse than anything currently in the good array
	if (votes.length) {
		submitVote(votes);
	}

	//add both pairs to the 'bad' array
	badSamples.push(badSampleL);
	badSamples.push(badSampleR);

	//throw out oldest samples if there are more than 'maxSamplesToHold'
	badSamples = badSamples.slice(0 - maxSamplesToHold);

	cycleColors();
}

function voteBothGood() {
	var votes = [];
	var goodSampleL = {
		font : lhs_font,
		background : lhs_background
	};
	var goodSampleR = {
		font : rhs_font,
		background : rhs_background
	};

	//push the 'cartesian product' of the two new good samples and existing bad samples onto 'votes'
	badSamples.forEach(
		function(badSample) {
			votes.push ({
				bad : badSample,
				good : goodSampleL
			});

			votes.push ({
				bad : badSample,
				good : goodSampleR
			});
		}
	);

	//submit to the server that these colors are better than anything currently in the bad array
	if (votes.length) {
		submitVote(votes);
	}

	//add both pairs to the 'good' array
	goodSamples.push(goodSampleL);
	goodSamples.push(goodSampleR);

	//throw out oldest samples if there are more than 'maxSamplesToHold'
	goodSamples = goodSamples.slice(0 - maxSamplesToHold);

	cycleColors();
}

function cycleColors() {

	//update global variables
	lhs_font = getRandomColor();
	lhs_background = getRandomColor();
	rhs_font = getRandomColor();
	rhs_background = getRandomColor();

	//make changes to DOM
	lhs = document.getElementById('lhs');
	lhs.style.background = '#' + lhs_background;
	lhs.style.color = '#' + lhs_font;

	rhs = document.getElementById('rhs');
	rhs.style.background = '#' + rhs_background;
	rhs.style.color = '#' + rhs_font;
}

//get a base16 value between '00' and 'FF'
function getRandomHex() {
	var integer = Math.floor(Math.random() * 256);

	//convert to base16 padded with '0's if required
	return ("00" + integer.toString(16)).substr(-2,2);
}

function getRandomColor() {
	return getRandomHex() + getRandomHex() + getRandomHex();
}

